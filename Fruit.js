var fruit = [ ["Coconut", 3.99], ["Strawberry's", 3.00], ["Bannana", 2.50], ["Apple", 1.00], ["Orange", 1.20]]

//Displays all items in 1 line of text
console.log(fruit)

//Displays the array using a for of loop
for (let i = 0; i < 5; i++){
    console.log(fruit[i])
}

//Display array using a for loop
for (let x in fruit){
    console.log(fruit[x])
   }

   console.log(fruit[2])

   console.log(fruit[2][0])

   console.log(fruit[2][1])

   console.log(`${fruit[0][0]} ${fruit[0][1]}`)
   console.log(`${fruit[1][0]} ${fruit[1][1]}`)
   console.log(`${fruit[2][0]} ${fruit[2][1]}`)
   console.log(`${fruit[3][0]} ${fruit[3][1]}`)
   console.log(`${fruit[4][0]} ${fruit[4][1]}`)